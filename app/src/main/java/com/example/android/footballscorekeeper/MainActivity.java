package com.example.android.footballscorekeeper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    int scoreA = 0;
    int redCardA = 0;
    int yellowCardA = 0;
    int scoreB = 0;
    private int redCardB = 0;
    private int yellowCardB = 0;

    TextView scoreATextView;
    TextView scoreBTextView;
    TextView redCardATextView;
    TextView redCardBTextView;
    TextView yellowCardATextView;
    TextView yellowCardBTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        scoreATextView = (TextView) findViewById(R.id.team_a_score);
        scoreBTextView = (TextView) findViewById(R.id.team_b_score);
        redCardATextView = (TextView) findViewById(R.id.team_a_redCard);
        redCardBTextView = (TextView) findViewById(R.id.team_b_redCard);
        yellowCardATextView = (TextView) findViewById(R.id.team_a_yellowCard);
        yellowCardBTextView = (TextView) findViewById(R.id.team_b_yellowCard);
    }

    public void incScoreTeamA(View v) {
        scoreA++;
        refreshValuesOnScreen();
    }

    public void incScoreTeamB(View v) {
        scoreB++;
        refreshValuesOnScreen();
    }

    public void incYellowTeamA(View v) {
        if (yellowCardA == 1) {
            yellowCardA = 0;
            redCardA++;
        } else {
            yellowCardA++;
        }
        refreshValuesOnScreen();
    }

    public void incYellowTeamB(View v) {
        if (yellowCardB == 1) {
            yellowCardB = 0;
            redCardB++;
        } else {
            yellowCardB++;
        }
        refreshValuesOnScreen();
    }

    public void incRedTeamA(View v) {
        redCardA++;
        refreshValuesOnScreen();
    }

    public void incRedTeamB(View v) {
        redCardB++;
        refreshValuesOnScreen();
    }


    private void refreshValuesOnScreen() {
        scoreATextView.setText(String.valueOf(scoreA));
        scoreBTextView.setText(String.valueOf(scoreB));
        redCardATextView.setText(String.valueOf(redCardA));
        redCardBTextView.setText(String.valueOf(redCardB));
        yellowCardATextView.setText(String.valueOf(yellowCardA));
        yellowCardBTextView.setText(String.valueOf(yellowCardB));
    }

    public void onResetButtonClick(View v) {
        scoreA = 0;
        scoreB = 0;
        yellowCardA = 0;
        yellowCardB = 0;
        redCardA = 0;
        redCardB = 0;
        refreshValuesOnScreen();
    }

}
